USE trofimowova;

#1. Вывести все департаменты, отсортированные по алфавиту

SELECT * FROM departments ORDER BY dept_name;

#2. Вывести все департаменты, отсортированные по алфавиту в обратном порядке

SELECT * FROM departments ORDER BY dept_name DESC;

#3. Вывести не больше 1000 записей о рабочих с именами, начинающимися на 'D', и фамилиями, заканчивающимися на 'u'
SELECT * FROM employees WHERE first_name LIKE 'D%' and last_name LIKE '%u' LIMIT 1000;

#4. Вывести не больше 1000 записей о рабочих, которые рождены в период между 30.01.1960 и 12.05.1970
SELECT * FROM employees WHERE birth_date BETWEEN '1960-01-30' and '1970-05-12' LIMIT 1000;

#5. Вывести не больше 1000 записей о рабочих женского пола, которые были наняты на работы после 01.01.1990
SELECT * FROM employees WHERE gender = 'F' and hire_date > '1970-05-12' LIMIT 1000;

#6. Посчитать количество департаментов
SELECT COUNT(*) FROM departments;

#7. Найти минимальную, максимальную, среднюю зарплату, а также общую сумму выплат по зарплат в период кризисного 1998 года
SELECT MIN(salary) AS MinimalSalary,MAX(salary) AS MaximumSalary, AVG(salary) AS AverageSalary, SUM(salary) AS TotalSalary FROM salaries WHERE from_date >='1998-01-01' and to_date <= '1998-12-31';

#8. Вывести 1000 фамилий и имен рабочих, которые были наняты в разные дни
SELECT  first_name,last_name FROM (SELECT DISTINCT first_name, last_name,hire_date FROM employees)  AS hired_gun;

#9. Посчитать сколько раз встречается каждая из фамилий
SELECT  last_name,COUNT(last_name) FROM employees GROUP BY last_name;

#10. Вывести фамилии и их число повторений (число повторений должно превышать 200)
SELECT last_name,COUNT(last_name) FROM employees GROUP BY last_name HAVING COUNT(last_name)>200; 
